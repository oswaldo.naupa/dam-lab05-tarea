/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import * as React from 'react';
import { Text, View, Button,TouchableOpacity, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Details from './app/components/Details/Details' 
import ConexionFetch from './app/components/conexionFetch/ConexionFetch'
import TopNavigation from './app/components/menu/TopNavigation'
import Login from './app/components/Login/Login'
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';

function LoginScreen({ navigation }) {
  return (
    <View>
      <Login navigation={navigation}></Login>
    </View>
    
  );
}
function HomeScreen({ navigation }) {
  return (
    <ConexionFetch navigation={navigation}></ConexionFetch>
  );
}

function DetailsScreen({ route, navigation }) {
  /* 2. Get the param */
  const { title } = route.params;
  const { image } = route.params;
  const { summary } = route.params;

  const titulo = JSON.stringify(title)
  const imagen = JSON.stringify(image)
  const resumen = JSON.stringify(summary)

  return (
    <View>
      <Details titulo={titulo} navigation={navigation} imagen={imagen} resumen={resumen} ></Details>     
    </View>
  );
}

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  _menu = null;
 
  setMenuRef = ref => {
    this._menu = ref;
  };
 
  hideMenu = () => {
    this._menu.hide();
  };
 
  showMenu = () => {
    this._menu.show();
  };
  
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login" drawerContent={(props)=><Menu {...props}/>}>
      <Stack.Screen name="Login" component={LoginScreen} 
        options={{
          title: 'Login',
          headerShown: false
        }}
        />
      <Stack.Screen name="Home" component={HomeScreen} 
        options={({navigation}) => (
          { 
          headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
          title: 'Lista de pelculas',
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerLeft:null,
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 30,
          },
        }
      )}
        />
      <Stack.Screen name="Details" component={DetailsScreen} 
        options={({navigation}) => (
          {
          headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
          title: 'Detalles pelicula',
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 30,
          },
        }
        )}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
  
}
const styles = StyleSheet.create({
  
  textSub: {
      width:'70%', 
      textAlignVertical:'center',
      color: '#f4511e',
      textAlign:'justify',
      fontSize: 20,
   
  },
  textTitle:{
      fontWeight: 'bold',
  },
  textView: {
      textAlignVertical:'center',
      color: '#c43c00',
      textAlign:'justify',
      padding: 10
   
  },
  text: {
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: 30,
    color: '#f4511e',
  },
  image: {
      alignItems: 'center',
      justifyContent: 'center',
      padding: 20,
    },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#f4511e',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
  textButon: {
      alignItems: 'center',
      padding: 5,
      fontWeight: 'bold',
      fontSize: 20,
      color: '#fff',
    },
})

import React, {Component} from 'react';
import {View, FlatList, StyleSheet, Text, Image, Button,Dimensions,TouchableOpacity  } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'


var { height } = Dimensions.get('window');
export default class Details extends Component{
    
    constructor(props){
        super(props);        
    }
    render(){

        var url = this.props.imagen
        var url2= url.substring(1,)
        var url3=url2.substring(0,url2.length-1)
        return(
            <View style={styles.container}>
                <Text style={styles.text}>{this.props.titulo}</Text>
                <View style={styles.image}>
                <Image  source = {{uri: url3}} style={{height: 300, width: 300}}/>
                </View>
                <Text style={styles.textSub}>Resumen:</Text>  
                <Text style={styles.textView}>{this.props.resumen}</Text>
                <TouchableOpacity   style={styles.button}   onPress={() => {
          /* 1. Navigate to the Details route with params */
          this.props.navigation.navigate('Home');
        }}>
            <Text style={styles.textButon}>Volver</Text>
            </TouchableOpacity >     
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        backgroundColor:'#FFCCBC',
        padding: 20,
        paddingBottom: 150,
        height: height
    },
    textSub: {
        width:'70%', 
        textAlignVertical:'center',
        color: '#f4511e',
        textAlign:'justify',
        fontSize: 20,
     
    },
    textTitle:{
        fontWeight: 'bold',
    },
    textView: {
        textAlignVertical:'center',
        color: '#c43c00',
        textAlign:'justify',
        padding: 10
     
    },
    text: {
      alignItems: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      color: '#f4511e',
    },
    image: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
      },
    button: {
      top: 10,
      alignItems: 'center',
      backgroundColor: '#f4511e',
      padding: 10,
    },
    countContainer: {
      alignItems: 'center',
      padding: 10,
    },
    countText: {
      color: '#FF00FF',
    },
    textButon: {
        alignItems: 'center',
        padding: 5,
        fontWeight: 'bold',
        fontSize: 20,
        color: '#fff',
      },
  })

/*function Item ({title,image,route}) {
    const otherParam = route.params;
    //Console.log(JSON.stringify(otherParam));
    return (
        <View style= {styles.item}>
            <Image source = {{uri: image}} style={{height: 40, width: 40}}/>
            <Text style= {styles.title}>{title}</Text>
        </View>
    );
}

export default class Details extends Component {
    constructor(props) {
        super(props);

        this.state = {  
            textValue: 0,
            count: 0,
            items: [],
            error: null,
        };
    }
    

    async componentDidMount(){
        await fetch('https://yts.mx/api/v2/list_movies.json')
            .then(res => res.json())
            .then(
                result => {
                    console.warn('result', result.data.movies);
                    this.setState({
                        items: result.data.movies,
                    });
                },
                error => {
                    this.setState({
                        error: error,
                    });
                },
            );
    }
    render() {
        return (
            <View style={styles.container}>
                <FlatList 
                    data={this.state.items.length > 0 ? this.state.items: []}
                    renderItem={({item}) =>(
                        <Item title={item.title}  image={item.small_cover_image} router={this.props.router}/>
                    )}
                    keyExtractor={item =>item.id}
                />   
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 20,
    },
    item: {
      backgroundColor: 'gray',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
    title: {
      fontSize: 32,
    },
  });*/
import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity,TextInput,Dimensions, Text, View, Image} from 'react-native';

var { height } = Dimensions.get('window');
export default class Login extends Component{
    
    constructor(props){
        super(props);        
        this.state ={
            UsuarioValue: '',
            ContraseñaValue: '',
            count: 0,
          };
        }
      
        changeTextInput = text => {
          this.setState ({UsuarioValue: text});
        };
        changeTextInputContraseña = text => {
          this.setState ({ContraseñaValue: text});
        };
        CheckTextInput = () => {
          const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
         //const clave=/^(?=.*[A-Za-z])(?=.*\D)[A-Za-z\d]{8,}$/;
         const clave=/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,}$/;
          //Handler for the Submit onPress
          if (this.state.UsuarioValue != '') {
            //Check for the Name TextInput
            if (this.state.ContraseñaValue != '') {
              //Check for the Email TextInput
              if(this.state.ContraseñaValue.length>=8){ 
                if(reg.test(this.state.UsuarioValue) === true){
                  if(clave.test(this.state.ContraseñaValue)==true){
                    this.props.navigation.navigate('Home');
                    this.setState ({UsuarioValue: '',ContraseñaValue: ''});
                  }else{
                    alert('La clave debe tener como minimo un numero, una letra mayuscula y una minuscula')
                  }
                }else{
                  alert('usuario no valido')
                }
              }else{
                alert('La contraseña debe tener minimo 8 caracteres')
              }
            } else {
              alert('Por favor, Ingrese su Contraseña');
            }
          } else {
              alert('Por favor, Ingrese su Usuario'); 
          }
        };
    render(){

        return (
            <View style={styles.container}>
              
              <View style={styles.image}>
                <Image  source={require('./img/logo3.png')}/>
              </View>
              <View>
                <Text style={styles.text}> Ingrese su Usuario:</Text>
              </View>
              <TextInput 
                style={styles.textInput}
                onChangeText= {text=> this.changeTextInput(text)}
                placeholder="Correo del usuario"
                placeholderTextColor="#20b2aa"
                value= {this.state.UsuarioValue}
              />
               <View >
                <Text style={styles.text}> Ingrese su Contraseña:</Text>
              </View>
              <TextInput 
                style={styles.textInput}
                placeholder="Contraseña"
                placeholderTextColor="#20b2aa"
                secureTextEntry
                onChangeText= {text=> this.changeTextInputContraseña(text)}
                value= {this.state.ContraseñaValue}
              />
              <TouchableOpacity style={styles.button} onPress={this.CheckTextInput}>
                <Text style={styles.textButon}>Login</Text>
              </TouchableOpacity>
            </View>
          );
        }
      }

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor:'darkblue',
    padding: 100,
    paddingBottom: 250,
    height: height
  },
  text: {
    alignItems: 'center',
    padding: 10,
    fontWeight: 'bold',
    color: '#DDDDDD',
    fontSize: 20,
  },
  image: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    borderRadius : 30,
  },
  textInput: {
    height: 40, 
    borderColor: '#DDDDDD', 
    borderWidth: 1,
    fontSize: 18,
    color: '#DDDDDD',
    borderRadius : 10,
  },
  textButon: {
    alignItems: 'center',
    padding: 5,
    fontWeight: 'bold',
    fontSize: 20,
    
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
})

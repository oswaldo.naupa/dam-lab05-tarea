import React, {Component} from 'react';
import {View, FlatList, StyleSheet,RefreshControl, Text,TouchableOpacity , Image,Button } from 'react-native';
import { SearchBar } from 'react-native-elements';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


function Item ({title,image,navigation,summary,largeimage}) {
    return (
        <View style= {styles.container}>
        <View style={{flex:1, flexDirection: 'row'}}>
            <Image style={styles.imageView} source = {{uri: largeimage}}/>
            <View style= {styles.textView}>
            <Text style= {styles.title}>{title}</Text>
            </View>
            <View style={styles.textvolver}>
            <TouchableOpacity  title="Detalles"  onPress={() => {
                    /* 1. Navigate to the Details route with params */
                    navigation.navigate('Details', {
                        summary: summary,
                        title: title,
                        image:largeimage,
                    });
                    }}>
            <Image source={require('./boton1.png') } style={{height: 40, width: 40}}/>
        </TouchableOpacity >
        </View>
        </View>
        </View>
    );
}
export default class ConexionFetch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            textValue: 0,
            count: 0,
            items: [],
            error: null,
            searchText: "",
            text: "",
            data:[],
            loading: true,
            refreshing: true,
        };
    }

    searchFilterFunction = text => {
            
            const newData = this.state.items.filter(item => {      
            const itemData = `${item.title.toUpperCase()}`;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;    
        });
        
        this.setState({ data: newData });
        this.setState({text: text})  
      };
    renderHeader = () => {    
        return (      
          <SearchBar        
            placeholder="Type Here..."        
            lightTheme        
            round        
            onChangeText={text => this.searchFilterFunction(text)}
            value= {this.state.text}
            autoCorrect={false}             
          />    
        );  
      };

    cargarDatos = () =>{
        this.setState({loading: true})
        return fetch('https://yts.mx/api/v2/list_movies.json')
            .then(res => res.json())
            .then(
                result => {
                    console.warn('result', result.data.movies);
                    this.setState({
                        items: result.data.movies,
                        data: result.data.movies,
                        loading: false,
                        refreshing: false, 
                    });
                },
                error => {
                    this.setState({
                        error: error,
                        loading: false,
                        refreshing: false, 
                    });
                },
            );
    }
    onRefresh() {
        //Clear old data of the list
        this.setState({ data: [] });
        this.cargarDatos()
        //Call the Service to get the latest data
        
      }

    async componentDidMount(){
        this.cargarDatos()
    }

    renderSeparatorView = () => {
        return (
          <View style={{
              height: 2, 
              width: "100%",
              backgroundColor: "#f4511e",
            }}
          />
        );
      };
    

    render() {
        {
        if(this.state.loading)
        { 
            return (
            <View style={styles.loading}>
                <Text style={styles.titleloading}>Cargando...</Text>
            </View>);
        }

        return (
            <View style={styles.container}>
                <FlatList
                    extraData={this.state}
                    data={this.state.data.length > 0 ? this.state.data: []}
                    renderItem={({item}) =>(
                        <Item title={item.title} image={item.small_cover_image} navigation={this.props.navigation} summary={item.summary} largeimage={item.large_cover_image}/>
                    )}
                    refreshControl={
                        <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this.onRefresh.bind(this)}
                        />}
                    keyExtractor={item =>item.id}
                    ListHeaderComponent={this.renderHeader}
                    ItemSeparatorComponent={this.renderSeparatorView}
                />
            </View>
        );
        }
        
    }
}
const styles = StyleSheet.create({
    loading: {
        flex: 1,
        padding:50,
        justifyContent: 'center',
        alignContent: 'center',
      },
    container: {
      flex: 1,
      marginTop: 15,
      margin: 20
    },
    imageView: {
 
        width: '30%',
        height: 90 ,
        margin: 20,
        borderRadius : 30, 
    },
    textView: {
        width:'45%', 
        textAlignVertical:'center',
        textAlign:'justify',
        justifyContent: 'center',
        fontFamily: 'Pacifico-Regular',
    },
    textvolver:{
        width: '25%',
        justifyContent: 'center'
        
    },
    item: {
      backgroundColor: 'gray',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 8,
    },
    title: {
      fontSize: 20,
      color: '#c43c00',
      fontWeight: 'bold',
      fontFamily: 'Bangers',
    },
    titleloading: {
        fontSize: 40,
        color: '#c43c00',
        fontWeight: 'bold',
        fontFamily: 'Bangers',
      },
  });
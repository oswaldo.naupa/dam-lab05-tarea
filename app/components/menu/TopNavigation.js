
import React, {Component} from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import Icon from 'react-native-vector-icons/MaterialIcons';

class TopNavigation  extends Component{
    constructor(props){
        super(props);        
    }
  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this.props.navigation.navigate('Home')
  };

  showMenu = () => {
    this._menu.show();
  };

  render() {
    return (
      <View style={{lignItems: 'center', justifyContent: 'center' }}>
          <Menu
          ref={this.setMenuRef}
          button={<Icon
            Icon name="menu" size={40} color="#fff" onPress={this.showMenu}></Icon>}
          >
            <MenuItem><Text style={styles.textTitle}>Ajustes</Text></MenuItem>
            <MenuItem onPress={this.hideMenu}><Text style={styles.textTitle}>Opciones</Text></MenuItem>
            <MenuItem onPress={this.hideMenu} disabled><Text style={styles.textTitle}>Ayuda</Text>
            </MenuItem>
              <MenuDivider />
            <MenuItem onPress={() => {
                    /* 1. Navigate to the Details route with params */
                    this.props.navigation.navigate('Login');
                  }}><Text style={styles.textTitle}>Cerrar Sesion</Text></MenuItem>
          </Menu>
      </View>
    );
  }
}

export default TopNavigation ;

const styles = StyleSheet.create({
  textTitle:{
      fontWeight: 'bold',
      fontSize: 20,
  },
})
